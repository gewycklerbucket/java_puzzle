import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int plcsNumber = 1100;
        int empNumer = 1100;

        System.out.println(countPlcsInRunMode(startInauguralProcess(createPlcList(plcsNumber), empNumer)));
    }

    public static Mode[] createPlcList(int arrLength) {
        Mode[] arr = new Mode[arrLength];
        Arrays.fill(arr, Mode.RUN);
        return arr;
    }

    public static Mode[] startInauguralProcess(Mode[] allPlcs, int numberOfEmployee) {
        for (int emp = 1; emp < numberOfEmployee; emp++) {
            for (int plc = emp; plc < allPlcs.length; plc += emp + 1) {
                if (allPlcs[plc] == Mode.PROGRAM) {
                    allPlcs[plc] = Mode.RUN;
                } else {
                    allPlcs[plc] = Mode.PROGRAM;
                }
            }
        }
        return allPlcs;
    }

    public static int countPlcsInRunMode(Mode[] allPlcs) {
        int runModeNumber = 0;
        for (int i = 0; i < allPlcs.length; i++) {
            if (allPlcs[i] == Mode.RUN) {
                runModeNumber++;
            }
        }
        return runModeNumber;
    }
}
